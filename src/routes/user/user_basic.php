<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Get user
$app->get('/api/user/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	if($id == null)
	{
		echo msg_err_ParamNotSet();
		return;
	}

	echo GetUserById($id);
});

// Add user
$app->post('/api/user/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["email"]) || empty($_POST["password"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$email = $request->getParam('email');
	$password = $request->getParam('password');

	echo AddUser($email, $password);
});

// Check email exist
$app->post('/api/user/email_check', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["email"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$email = $request->getParam('email');

	if(!ValidateEmail($email))
	{
		echo msg_fail_InvalidEmail();
		return;
	}

	if(!CheckUserEmailAvailable($email))
	{
		echo msg_fail_EmailInUse();
	}
	else
	{
		echo msg_scs_EmailAvailable();
	}
});

// Update user password
$app->post('/api/user/password_update', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["email"]) || empty($_POST["current_password"]) || empty($_POST["new_password"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$current_password = $request->getParam('current_password');
	$new_password = $request->getParam('new_password');

	echo UpdateUserPassword($id, $current_password, $new_password);
});

// Update user email
$app->post('/api/user/email_update', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["email"]) || empty($_POST["new_email"]) || empty($_POST["password"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$new_email = $request->getParam('new_email');
	$password = $request->getParam('password');

	echo UpdateUserEmail($id, $new_email, $password);
});

// De/activate account
$app->post('/api/user/activate', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id= $request->getParam('id');
	$active = $request->getParam('active');

	echo ActivateUser($id, $active);
});
?>
