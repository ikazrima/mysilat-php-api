<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use JSend\JSendResponse;

$app->post('/api/user/profile/assign', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"]) || empty($_POST["basic_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$basic_id = $request->getParam('basic_id');

	echo AssignUserProfileBasicId($id, $basic_id);
});

$app->get('/api/user/profile/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetUserProfileById($id);
});

$app->get('/api/user/profile/fb/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetUserProfileByFBId($id);
});

$app->get('/api/user/profile/basic_id/{basic_id}', function (Request $request, Response $response, array $args)
{
	$basic_id = $request->getAttribute('basic_id');

	echo GetUserProfileByBasicId($basic_id);
});

$app->get('/api/user/profile/ic/{identification}', function (Request $request, Response $response, array $args)
{
	$identification = $request->getAttribute('identification');

	echo GetUserProfileByIdentification($identification);
});

$app->post('/api/user/profile/email', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["email"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$email = $request->getParam('email');

	echo GetUserProfileByEmail($email);
});

// Update user names
$app->post('/api/user/profile/update_names', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"])
	|| empty($_POST["first_name"])
	|| empty($_POST["last_name"])
	|| empty($_POST["alternate_name"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$first_name = $request->getParam('first_name');
	$last_name = $request->getParam('last_name');
	$alternate_name = $request->getParam('alternate_name');

	echo UpdateUserProfileNames($id, $first_name, $last_name, $alternate_name);
});

// Update user identification
// SHOULD NOT BE ACCESSIBLE by default user
$app->post('/api/user/profile/update_identification', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"])
	|| empty($_POST["identification"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$identification = $request->getParam('identification');

	echo UpdateUserProfileIdentification($id, $identification);
});

// Get user identification
// SHOULD ONLY BE ACCESSIBLE BY CURRENT USER
$app->get('/api/user/profile/identification/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetUserIdentification($id);
});
?>
