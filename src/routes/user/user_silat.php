<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use JSend\JSendResponse;

// Get silat details
$app->get('/api/user/silat/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetUserSilatById($id);
});

// Get silat details
$app->get('/api/user/silat/user_id/{user_id}', function (Request $request, Response $response, array $args)
{
	$user_id = $request->getAttribute('user_id');

	echo GetUserSilatByUserId($user_id);
});

// Add silat entry
$app->post('/api/user/silat/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

  $user_id = $request->getParam('user_id');

	echo AddUserSilat($user_id);
});

// Update peringkat silat
$app->post('/api/user/silat/update_kelas', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]) || empty($_POST["main_kelas_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$main_kelas_id = $request->getParam('main_kelas_id');

  echo UpdateUserSilatMainKelas($user_id, $main_kelas_id);
});

// Update peringkat silat
$app->post('/api/user/silat/update_peringkat', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]) || empty($_POST["peringkat_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$peringkat_id = $request->getParam('peringkat_id');

  echo UpdateUserSilatPeringkat($user_id, $peringkat_id);
});

// Update ijazah dates
$app->post('/api/user/silat/update_ijazah', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["ijazah_asas"])
	|| empty($_POST["ijazah_potong"])
	|| empty($_POST["ijazah_tamat"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$ijazah_asas = $request->getParam('ijazah_asas');
  $ijazah_potong = $request->getParam('ijazah_potong');
  $ijazah_tamat = $request->getParam('ijazah_tamat');

	echo UpdateUserSilatIjazah($user_id, $ijazah_asas, $ijazah_potong, $ijazah_tamat);
});

$app->post('/api/user/silat/update_asas', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["date"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$date = $request->getParam('date');

	echo UpdateUserSilatIjazahAsas($user_id, $date);
});

$app->post('/api/user/silat/update_potong', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["date"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$date = $request->getParam('date');

	echo UpdateUserSilatIjazahPotong($user_id, $date);
});

$app->post('/api/user/silat/update_tamat', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["date"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$date = $request->getParam('date');

	echo UpdateUserSilatIjazahTamat($user_id, $date);
});

// Update sijil dates
$app->post('/api/user/silat/update_sijil', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$srp = $request->getParam('srp');
  $smp = $request->getParam('smp');

  echo UpdateUserSilatSijil($user_id, $srp, $smp);
});

$app->post('/api/user/silat/update_srp', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["date"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$date = $request->getParam('date');

  echo UpdateUserSilatSRP($user_id, $date);
});

$app->post('/api/user/silat/update_smp', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["date"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$date = $request->getParam('date');

  echo UpdateUserSilatSMP($user_id, $date);
});
?>
