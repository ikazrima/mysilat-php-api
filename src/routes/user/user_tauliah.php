<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use JSend\JSendResponse;

// Get tauliah details by user id
$app->get('/api/user/tauliah/user_id/{user_id}', function (Request $request, Response $response, array $args)
{
	$user_id = $request->getAttribute('user_id');

	echo GetUserTauliahByUserId($user_id);
});

// Get tauliah details by user id AND year
$app->post('/api/user/tauliah/user_year', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
		|| empty($_POST["year"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$year = $request->getParam('year');

  echo GetUserTauliahByUserIdYear($user_id, $year);
});

// Get tauliah details by kelas id AND year
$app->post('/api/user/tauliah/kelas_year', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["kelas_id"])
		|| empty($_POST["year"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$kelas_id = $request->getParam('kelas_id');
	$year = $request->getParam('year');

  echo GetAllUserTauliahByKelasIdYear($kelas_id, $year);
});

// Add tauliah entry
$app->post('/api/user/tauliah/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
		|| empty($_POST["year"])
		|| empty($_POST["kelas_id"])
		|| empty($_POST["tauliah_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$year = $request->getParam('year');
	$kelas_id = $request->getParam('kelas_id');
  $tauliah_id = $request->getParam('tauliah_id');

  echo AddUserTauliah($user_id, $year, $kelas_id, $tauliah_id);
});

// Approve tauliah
$app->post('/api/user/tauliah/approve', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"])
		|| empty($_POST["approved_by"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$approved_by = $request->getParam('approved_by');

  echo ApproveUserTauliah($id, $approved_by);
});

// Update tauliah
$app->post('/api/user/tauliah/update_tauliah', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
		|| empty($_POST["year"])
		|| empty($_POST["kelas_id"])
		|| empty($_POST["tauliah_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$year = $request->getParam('year');
	$kelas_id = $request->getParam('kelas_id');
	$tauliah_id = $request->getParam('tauliah_id');

  echo UpdateUserTauliah($user_id, $year, $kelas_id, $tauliah_id);
});

// Activate tauliah
$app->post('/api/user/tauliah/activate', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
		|| empty($_POST["year"])
		|| empty($_POST["kelas_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$year = $request->getParam('year');
	$kelas_id = $request->getParam('kelas_id');
	$active = $request->getParam('active');

  echo ActivateUserTauliah($user_id, $year, $kelas_id, $active);
});

?>
