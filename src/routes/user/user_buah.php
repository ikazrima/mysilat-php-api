<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use JSend\JSendResponse;

// Get user buah list
$app->post('/api/user/buah', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]) || empty($_POST["peringkat_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$peringkat_id = $request->getParam('peringkat_id');

	echo GetUserBuah($user_id, $peringkat_id);
});

// Set buah json
$app->post('/api/user/buah/set', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]) || empty($_POST["peringkat_id"]) || empty($_POST["buah_json"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$peringkat_id = $request->getParam('peringkat_id');
	$buah_json = $request->getParam('buah_json');

	//$json = $request->getBody();

	echo SetUserBuah($user_id, $peringkat_id, $buah_json);
});

?>
