<?php

function GetUserTauliahByUserId($user_id)
{
  $sql = "SELECT *
    FROM
      user_tauliah
    WHERE
      user_id = :user_id
      AND approved = 1
      AND active = 1";
  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetUserTauliahByUserIdYear($user_id, $year)
{
  $sql = "SELECT *
    FROM
      user_tauliah
    WHERE
      user_id = :user_id
      AND approved = 1
      AND year = :year
      AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':year', $year);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetAllUserTauliahByKelasIdYear($kelas_id, $year)
{
  $sql = "SELECT *
  FROM
    user_tauliah
  WHERE
    kelas_id = :kelas_id
    AND approved = 1
    AND year = :year
    AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':kelas_id', $kelas_id);
    $stmt->bindParam(':year', $year);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function AddUserTauliah($user_id, $year, $kelas_id, $tauliah_id)
{
  if(CheckUserTauliahExist($user_id, $year, $kelas_id, 1))
  {
    return msg_fail_RecordExist();
  }

  $sql =
  "INSERT INTO
    user_tauliah (user_id, year, kelas_id, tauliah_id)
  VALUES
    (:user_id, :year, :kelas_id, :tauliah_id)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':year', $year);
		$stmt->bindParam(':kelas_id', $kelas_id);
    $stmt->bindParam(':tauliah_id', $tauliah_id);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function ApproveUserTauliah($id, $approved_by)
{
  $approved_dt = date("Y-m-d H:i:s");

	$sql =
  "UPDATE
    user_tauliah
  SET
    approved = 1,
		approved_by = :approved_by,
    approved_dt = :approved_dt
  WHERE
    id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
    $stmt->bindParam(':approved_by', $approved_by);
		$stmt->bindParam(':approved_dt', $approved_dt);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserTauliah($user_id, $year, $kelas_id, $tauliah_id)
{
  if(!CheckUserTauliahExist($user_id, $year, $kelas_id, 1))
  {
    return msg_fail_RecordDoesntExist();
  }

	$sql =
  "UPDATE
    user_tauliah
  SET
    kelas_id = :kelas_id,
		tauliah_id = :tauliah_id
  WHERE
    user_id = :user_id
		AND year = :year
		AND kelas_id = :kelas_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':year', $year);
		$stmt->bindParam(':kelas_id', $kelas_id);
		$stmt->bindParam(':tauliah_id', $tauliah_id);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function ActivateUserTauliah($user_id, $year, $kelas_id, $active)
{
  if(!CheckUserTauliahExist($user_id, $year, $kelas_id, -1))
  {
    return msg_fail_RecordDoesntExist();
  }

	$sql =
  "UPDATE
    user_tauliah
  SET
    active = :active
  WHERE
    user_id = :user_id
		AND year = :year
		AND kelas_id = :kelas_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':year', $year);
		$stmt->bindParam(':kelas_id', $kelas_id);
		$stmt->bindParam(':active', $active);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckUserTauliahExist($user_id, $year, $kelas_id, $active)
{
  $sql =
	"SELECT *
	FROM
		user_tauliah
	WHERE
		user_id = :user_id AND year = :year AND kelas_id = :kelas_id";

	if($active >= 0)
		$sql = $sql." AND active = :active";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':year', $year);
		$stmt->bindParam(':kelas_id', $kelas_id);

		if($active >= 0)
			$stmt->bindParam(':active', $active);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) >0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

?>
