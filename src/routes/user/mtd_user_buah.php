<?php

function GetUserBuah($user_id, $peringkat_id)
{
  $sql = "SELECT * FROM user_buah WHERE user_id = :user_id AND peringkat_id = :peringkat_id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':peringkat_id', $peringkat_id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;
    //return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }

  /////////////////////////////////////////
  // decoding json
  /////////////////////////////////////////
  $buah_json = $data[0]['buah_json'];
  $user_buah = json_decode($buah_json, true);

  $index = 0;

  foreach ($user_buah as $row)
  {
    $jurulatih_id = $row['jurulatih_id'];
    $sql = "SELECT id, alternate_name FROM user_profile WHERE id = :jurulatih_id";

    try
    {
      // Get DB Object
      $db = new db();

      // Connect
      $db = $db->connect();

      $stmt = $db->prepare($sql);

      $stmt->bindParam(':jurulatih_id', $jurulatih_id);

      $stmt->execute();

      $newData = $stmt->fetchAll(PDO::FETCH_ASSOC);

      $db = null;

      if(!$newData)
      {
        $user_buah[$index]['jurulatih_name'] = "";
      }
      else {
        $user_buah[$index]['jurulatih_name'] = $newData[0]['alternate_name'];
      }

      $newData = null;
    }
    catch(PDOException $e)
    {
      //return msg_err_Exception($e);
    }

    $kelas_id = $row['kelas_id'];
    $sql = "SELECT id, code FROM kelas_details WHERE id = :kelas_id";

    try
    {
      // Get DB Object
      $db = new db();

      // Connect
      $db = $db->connect();

      $stmt = $db->prepare($sql);

      $stmt->bindParam(':kelas_id', $kelas_id);

      $stmt->execute();

      $newData = $stmt->fetchAll(PDO::FETCH_ASSOC);

      $db = null;

      if(!$newData)
      {
        $user_buah[$index]['kelas_code'] = "";
      }
      else {
        $user_buah[$index]['kelas_code'] = $newData[0]['code'];
      }

      $newData = null;
    }
    catch(PDOException $e)
    {
      //return msg_err_Exception($e);
    }

    $index++;
  }

  $data[0]['buah_json'] = json_encode($user_buah);
  return msg_scs_Result($data);
}

function SetUserBuah($user_id, $peringkat_id, $buah_json)
{
  /*
  $buah = json_decode($buah_json, true);

  // JSON is invalid
	if (json_last_error() != JSON_ERROR_NONE) {
		return msg_err_InvalidJson();
	}


	$buah_json = json_encode($buah);
	$user_id = $buah[0]['user_id'];
  */

	$sql = '';

	if(CheckUserBuahExist($user_id, $peringkat_id))
	{
		// Update existing record
		$sql =
		"UPDATE
			user_buah
		SET
			buah_json = :buah_json
		WHERE
			user_id = :user_id
      AND peringkat_id = :peringkat_id";
	}
	else
	{
		// Insert new record
		$sql =
		"INSERT INTO
			user_buah (user_id, peringkat_id, buah_json)
		VALUES
			(:user_id, :peringkat_id, :buah_json)";
	}

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':peringkat_id', $peringkat_id);
		$stmt->bindParam(':buah_json', $buah_json);

		$stmt->execute();

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckUserBuahExist($user_id, $peringkat_id)
{
	$sql = "SELECT * FROM user_buah WHERE user_id = :user_id AND peringkat_id = :peringkat_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':peringkat_id', $peringkat_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) >0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

?>
