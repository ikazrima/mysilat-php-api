<?php
function AssignUserProfileBasicId($id, $basic_id)
{
	if(!CheckUserProfileExist($id))
	{
    return msg_fail_RecordDoesntExist();
  }

	if(CheckUserProfileAssigned($id, $basic_id))
	{
    return msg_fail_EmailInUse();
  }

	$sql = "UPDATE
	user_profile
	SET
	basic_id = :basic_id
	WHERE
	id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':basic_id', $basic_id);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserProfileById($id)
{
	$sql =
	"SELECT *
	FROM
		user_profile
	WHERE
		id = :id
		AND active = 1";

		$year = date("Y");

		$sql =
		"SELECT
			profile.id as profile_id,
			profile.sex,
			profile.first_name,
			profile.last_name,
			profile.alternate_name,
			user_silat.peringkat_id,
			silat_peringkat.peringkat,
			tauliah.tauliah_id,
			silat_tauliah.code as jawatan_code,
			silat_tauliah.jawatan,
			socmed.facebook_id,
			tauliah.year as tauliah_year,
			image.id as profile_img_id,
			image.img_name as profile_img_name,
			image.img_path as profile_img_uri
		FROM
			user_tauliah as tauliah
			INNER JOIN user_profile as profile
			ON profile.id = tauliah.user_id
			INNER JOIN silat_tauliah
			ON silat_tauliah.id = tauliah.tauliah_id
			LEFT JOIN social_media as socmed
			ON socmed.owner_id = profile.id
			INNER JOIN kelas_details as kelas
			ON kelas.id = tauliah.kelas_id
			INNER JOIN user_silat
			ON user_silat.user_id = profile.id
			INNER JOIN silat_peringkat
			ON silat_peringkat.id = user_silat.peringkat_id
			LEFT JOIN image
			ON profile.profile_img = image.id
		WHERE
			tauliah.kelas_id = user_silat.main_kelas_id
			AND tauliah.year = :year
			AND tauliah.active = 1
			AND profile.id = :id
			AND profile.active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':year', $year);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserProfileByBasicId($basic_id)
{
	$sql =
	"SELECT *
	FROM
		user_profile
	WHERE
		basic_id = :basic_id
		AND active = 1";

		$year = date("Y");

		$sql =
		"SELECT
			profile.id as profile_id,
			profile.sex,
			profile.first_name,
			profile.last_name,
			profile.alternate_name,
			user_silat.peringkat_id,
			silat_peringkat.peringkat,
			tauliah.tauliah_id,
			silat_tauliah.code as jawatan_code,
			silat_tauliah.jawatan,
			socmed.facebook_id,
			tauliah.year as tauliah_year,
			image.id as profile_img_id,
			image.img_name as profile_img_name,
			image.img_path as profile_img_uri
		FROM
			user_tauliah as tauliah
			INNER JOIN user_profile as profile
			ON profile.id = tauliah.user_id
			INNER JOIN silat_tauliah
			ON silat_tauliah.id = tauliah.tauliah_id
			LEFT JOIN social_media as socmed
			ON socmed.owner_id = profile.id
			INNER JOIN kelas_details as kelas
			ON kelas.id = tauliah.kelas_id
			INNER JOIN user_silat
			ON user_silat.user_id = profile.id
			INNER JOIN silat_peringkat
			ON silat_peringkat.id = user_silat.peringkat_id
			LEFT JOIN image
			ON profile.profile_img = image.id
		WHERE
			tauliah.kelas_id = user_silat.main_kelas_id
			AND tauliah.year = :year
			AND tauliah.active = 1
			AND profile.basic_id = :basic_id
			AND profile.active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':basic_id', $basic_id);
		$stmt->bindParam(':year', $year);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserProfileByIdentification($identification)
{
	if(!ValidateIdentification($identification))
	{
		return msg_fail_InvalidIdentification();
	}

	$identification = StringNumbersOnly($identification);

	$sql = "SELECT *
	FROM
		user_profile
	WHERE
		identification = :identification
		AND active = 1";

		$year = date("Y");

		$sql =
		"SELECT
			profile.id as profile_id,
			profile.sex,
			profile.first_name,
			profile.last_name,
			profile.alternate_name,
			user_silat.peringkat_id,
			silat_peringkat.peringkat,
			tauliah.tauliah_id,
			silat_tauliah.code as jawatan_code,
			silat_tauliah.jawatan,
			socmed.facebook_id,
			tauliah.year as tauliah_year,
			image.id as profile_img_id,
			image.img_name as profile_img_name,
			image.img_path as profile_img_uri
		FROM
			user_tauliah as tauliah
			INNER JOIN user_profile as profile
			ON profile.id = tauliah.user_id
			INNER JOIN silat_tauliah
			ON silat_tauliah.id = tauliah.tauliah_id
			LEFT JOIN social_media as socmed
			ON socmed.owner_id = profile.id
			INNER JOIN kelas_details as kelas
			ON kelas.id = tauliah.kelas_id
			INNER JOIN user_silat
			ON user_silat.user_id = profile.id
			INNER JOIN silat_peringkat
			ON silat_peringkat.id = user_silat.peringkat_id
			LEFT JOIN image
			ON profile.profile_img = image.id
		WHERE
			tauliah.kelas_id = user_silat.main_kelas_id
			AND tauliah.year = :year
			AND tauliah.active = 1
			AND profile.identification = :identification
			AND profile.active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':identification', $identification);
		$stmt->bindParam(':year', $year);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserProfileByEmail($email)
{
		$year = date("Y");

		$sql =
		"SELECT
			profile.id as profile_id,
			profile.sex,
			profile.first_name,
			profile.last_name,
			profile.alternate_name,
			user_silat.peringkat_id,
			silat_peringkat.peringkat,
			tauliah.tauliah_id,
			silat_tauliah.code as jawatan_code,
			silat_tauliah.jawatan,
			socmed.facebook_id,
			tauliah.year as tauliah_year,
			image.id as profile_img_id,
			image.img_name as profile_img_name,
			image.img_path as profile_img_uri
		FROM
			user_tauliah as tauliah
			INNER JOIN user_profile as profile
			ON profile.id = tauliah.user_id
			INNER JOIN silat_tauliah
			ON silat_tauliah.id = tauliah.tauliah_id
			LEFT JOIN social_media as socmed
			ON socmed.owner_id = profile.id
			INNER JOIN kelas_details as kelas
			ON kelas.id = tauliah.kelas_id
			INNER JOIN user_silat
			ON user_silat.user_id = profile.id
			INNER JOIN silat_peringkat
			ON silat_peringkat.id = user_silat.peringkat_id
			LEFT JOIN image
			ON profile.profile_img = image.id
			INNER JOIN user_basic
			ON user_basic.id = profile.id
		WHERE
			tauliah.kelas_id = user_silat.main_kelas_id
			AND user_basic.email = :email
			AND tauliah.year = :year
			AND tauliah.active = 1
			AND profile.active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':year', $year);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserProfileByFBId($fb_id)
{
	$sql = "SELECT
	socmed.owner_id as id,
	profile.basic_id as basic_id,
	profile.identification,
	profile.first_name,
	profile.last_name,
	profile.alternate_name,
	profile.sex,
	profile.profile_img,
	profile.active,
	profile.created_dt,
	profile.modified_dt
		FROM
			user_profile as profile
			INNER JOIN social_media as socmed
			ON socmed.owner_id = profile.id
		WHERE
			profile.active = 1
			AND socmed.owner_type = '1'
			AND socmed.facebook_id = :fb_id";

			$year = date("Y");

			$sql =
			"SELECT
				profile.id as profile_id,
				profile.sex,
				profile.first_name,
				profile.last_name,
				profile.alternate_name,
				user_silat.peringkat_id,
				silat_peringkat.peringkat,
				tauliah.tauliah_id,
				silat_tauliah.code as jawatan_code,
				silat_tauliah.jawatan,
				socmed.facebook_id,
				tauliah.year as tauliah_year,
				image.id as profile_img_id,
				image.img_name as profile_img_name,
				image.img_path as profile_img_uri
			FROM
				user_tauliah as tauliah
				INNER JOIN user_profile as profile
				ON profile.id = tauliah.user_id
				INNER JOIN silat_tauliah
				ON silat_tauliah.id = tauliah.tauliah_id
				LEFT JOIN social_media as socmed
				ON socmed.owner_id = profile.id
				INNER JOIN kelas_details as kelas
				ON kelas.id = tauliah.kelas_id
				INNER JOIN user_silat
				ON user_silat.user_id = profile.id
				INNER JOIN silat_peringkat
				ON silat_peringkat.id = user_silat.peringkat_id
				LEFT JOIN image
				ON profile.profile_img = image.id
			WHERE
				tauliah.kelas_id = user_silat.main_kelas_id
				AND tauliah.year = :year
				AND tauliah.active = 1
				AND socmed.owner_type = '1'
				AND socmed.facebook_id = :fb_id
				AND profile.active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':fb_id', $fb_id);
		$stmt->bindParam(':year', $year);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserProfileNames($id, $first_name, $last_name, $alternate_name)
{
	if(!CheckUserProfileExist($id))
	{
		return msg_fail_RecordDoesntExist();
	}

	$sql =
	"UPDATE
		user_profile
	SET
		first_name = :first_name,
		last_name = :last_name,
		alternate_name = :alternate_name
	WHERE
		id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':first_name', $first_name);
		$stmt->bindParam(':last_name', $last_name);
		$stmt->bindParam(':alternate_name', $alternate_name);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserProfileIdentification($id, $identification)
{
	if(!ValidateIdentification($identification))
	{
		return msg_fail_InvalidIdentification();
	}

	$identification = StringNumbersOnly($identification);

	if(!CheckUserProfileIdentificationExist($identification))
	{
    return msg_fail_RecordDoesntExist();
  }

	$sql =
	"UPDATE
		user_profile
	SET
		identification = :identification
	WHERE
		id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':identification', $identification);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckUserProfileAssigned($id, $basic_id)
{
	$sql =
	"SELECT *
	FROM
		user_profile
	WHERE
		NOT id = :id
		AND basic_id = :basic_id
		AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':basic_id', $basic_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) >0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

function CheckUserProfileExist($id)
{
	$sql =
	"SELECT *
	FROM
		user_profile
	WHERE
		id = :id
		AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) >0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

function CheckUserProfileIdentificationExist($identification)
{
	if(!ValidateIdentification($identification))
	{
		echo msg_fail_InvalidIdentification();
		return;
	}

	$identification = StringNumbersOnly($identification);

	$sql =
	"SELECT *
	FROM
		user_profile
	WHERE
		identification = :identification
		AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':identification', $identification);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) >0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);;
	}
}

function GetUserIdentification($id)
{
	$sql =
	"SELECT identification
	FROM
		user_profile
	WHERE
		id = :id
		AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

?>
