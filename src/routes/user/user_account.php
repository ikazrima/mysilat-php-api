<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use JSend\JSendResponse;

// Login
$app->post('/api/account/login/email', function (Request $request, Response $response, array $args)
{
		if (empty($_POST["email"]) || empty($_POST["password"]))
		{
			echo msg_err_ParamNotSet();
			return;
		}

		$email = $request->getParam('email');
		$password = $request->getParam('password');

		echo LoginEmail($email, $password);
});

$app->post('/api/account/login/identification', function (Request $request, Response $response, array $args)
{
		if (empty($_POST["identification"]) || empty($_POST["password"]))
		{
			echo msg_err_ParamNotSet();
			return;
		}

		$identification = $request->getParam('identification');
		$password = $request->getParam('password');

		echo LoginIC($identification, $password);
});

// Register user
$app->post('/api/account/register', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["identification"])
	|| empty($_POST["first_name"])
	|| empty($_POST["last_name"])
	|| empty($_POST["alternate_name"])
	|| empty($_POST["email"])
	|| empty($_POST["password"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	// user basic
	$email = $request->getParam('email');
	$password = $request->getParam('password');

	// user profile
	$identification = $request->getParam('identification');
	$first_name = $request->getParam('first_name');
	$last_name = $request->getParam('last_name');
	$alternate_name = $request->getParam('alternate_name');

	echo RegisterNewUser($email, $password, $identification, $first_name, $last_name, $alternate_name);
});

// OBSOLETE, Don't call
// Add user profile
/*
$app->post('/api/user/profile/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["identification"])
	|| empty($_POST["first_name"])
	|| empty($_POST["last_name"])
	|| empty($_POST["alternate_name"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$identification = $request->getParam('identification');
	$first_name = $request->getParam('first_name');
	$last_name = $request->getParam('last_name');
	$alternate_name = $request->getParam('alternate_name');

	echo AddUserProfile($identification, $first_name, $last_name, $alternate_name);
});
*/

?>
