<?php

function LoginEmail($email, $password)
{
	if(!ValidateEmail($email))
		return msg_fail_InvalidEmail();

	if(VerifyUserPasswordByEmail($email, $password))
	{
		return msg_scs_Authenticated();
	}

	else
	{
		return msg_fail_InvalidLogin();
	}
}

function LoginIC($identification, $password)
{
  if(!ValidateIdentification($identification))
		return msg_fail_InvalidIdentification();

	if(VerifyUserPasswordByIC($identification, $password))
	{
		return msg_scs_Authenticated();
	}

	else
	{
		return msg_fail_InvalidLogin();
	}
}

function RegisterNewUser($email, $password, $identification, $first_name, $last_name, $alternate_name)
{
  $identification = StringNumbersOnly($identification);

  if(!ValidateIdentification($identification))
		return msg_fail_InvalidIdentification();

	if(CheckUserProfileIdentificationExist($identification))
		return msg_fail_IdentificationInUse();

  if(!ValidateEmail($email))
    return msg_fail_InvalidEmail();

  if(!CheckUserEmailAvailable($email))
    return msg_fail_EmailInUse();

  if(CheckUserSilatExist($user_id))
    return msg_fail_RecordExist();

  $sex = GetGenderFromIdentification($identification);
  $hashed_password = password_hash($password, PASSWORD_DEFAULT);

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();
    $db->beginTransaction();

    // Email & password
    $sql = "INSERT INTO user_basic (email, password) VALUES (:email, :password)";
    $stmt = $db->prepare($sql);

    $stmt->bindParam(':email', $email);
		$stmt->bindParam(':password', $hashed_password);
    $stmt->execute();

    $basic_id = $db->lastInsertId();

    // Profile
    $sql = "INSERT INTO user_profile (basic_id, identification, first_name, last_name, alternate_name, sex)
  	VALUES (:basic_id, :identification, :first_name, :last_name, :alternate_name, :sex)";
		$stmt = $db->prepare($sql);

    $stmt->bindParam(':basic_id', $basic_id);
		$stmt->bindParam(':identification', $identification);
		$stmt->bindParam(':first_name', $first_name);
		$stmt->bindParam(':last_name', $last_name);
		$stmt->bindParam(':alternate_name', $alternate_name);
		$stmt->bindParam(':sex', $sex);
    $stmt->execute();

    $profile_id = $db->lastInsertId();

    // User Silat
    $sql = "INSERT INTO user_silat (user_id) VALUES (:user_id)";
    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $profile_id);
    $stmt->execute();

    // Address book
    $sql = "INSERT INTO address_book (owner_id, type) VALUES (:owner_id, 1)";
    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $profile_id);
    $stmt->execute();

    // SocMed
    $sql = "INSERT INTO social_media (owner_id, owner_type) VALUES (:owner_id, 1)";
    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $profile_id);
    $stmt->execute();

    $db->commit();

    return msg_scs_RecordAdded();
	}
	catch(PDOException $e)
	{
    $db->rollback();
    echo msg_err_Exception($e);
    return false;
	}
}

function VerifyUserPasswordByIC($identification, $password)
{
  $sql ="SELECT
    user_basic.email,
    user_basic.password
  FROM
    user_profile
    INNER JOIN user_basic
    ON user_basic.id = user_profile.basic_id
  WHERE
    user_profile.identification = :identification
    AND user_profile.active = 1
    AND user_basic.active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);
    $stmt->bindParam(':identification', $identification);
    $stmt->execute();

    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    $db = null;

    $hashed_password = $data['password'];

    // Compare password
    if(password_verify($password, $hashed_password))
      return true;
    else
      return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function VerifyUserPasswordByEmail($email, $password)
{
  $sql = "SELECT * FROM user_basic WHERE email = :email AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':email', $email);

    $stmt->execute();

    $data = $stmt->fetch(PDO::FETCH_ASSOC);

    $db = null;

    $hashed_password = $data['password'];

    // Compare password
    if(password_verify($password, $hashed_password))
      return true;
    else
      return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}
?>
