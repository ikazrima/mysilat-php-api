<?php

function GetUserById($id)
{
	$sql = "SELECT id, email, created_dt FROM user_basic WHERE id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserIdByEmail($email, $active)
{
	$sql = "SELECT id FROM user_basic WHERE email = :email";

	if($active == 1)
	{
			$sql = $sql." AND active = 1";
	}

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':email', $email);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserEmailById($id)
{
	$sql = "SELECT email FROM user_basic WHERE id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function AddUser($email, $password)
{
	if(!ValidateEmail($email))
	{
		return msg_fail_InvalidEmail();
	}

	if(!CheckUserEmailAvailable($email))
	{
		return msg_fail_InvalidEmail();
	}

	$hashed_password = password_hash($password, PASSWORD_DEFAULT);

	$sql = "INSERT INTO user_basic (email, password) VALUES (:email, :password)";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':email', $email);
		$stmt->bindParam(':password', $hashed_password);

		$stmt->execute();

		return msg_scs_RecordAdded();;
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserPassword($id, $current_password, $new_password)
{
	if(!CheckUserExist($id))
	{
		msg_fail_RecordDoesntExist();
		return;
	}

	if(!VerifyUserPasswordById($id, $current_password))
	{
		return msg_fail_InvalidLogin();
	}

	$hashed_password = password_hash($new_password, PASSWORD_DEFAULT);

	$sql = "UPDATE user_basic SET password = :password WHERE id = :id";

	// Update password
	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':password', $hashed_password);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserEmail($id, $new_email, $password)
{
	if(!CheckUserExist($id))
	{
    return msg_fail_RecordDoesntExist();
  }

	if(!VerifyUserPasswordById($id, $password))
	{
		return msg_fail_InvalidLogin();
	}

	if(!ValidateEmail($new_email))
	{
		return msg_fail_InvalidEmail();
	}

	if(!CheckUserEmailAvailable($new_email))
	{
		return msg_fail_EmailInUse();
	}

	$sql = "UPDATE user_basic SET email = :new_email WHERE id = :id";

	// Update email
	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':new_email', $new_email);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function ActivateUser($id, $active)
{
	$result = GetUserById($id);

  $result = json_decode($result, true);
  $count = count($result['data']);

  if($count < 1)
  {
    return msg_fail_RecordDoesntExist();
  }

	// Check email availability when activating only
	if($active == 1)
	{
		$result = GetUserEmailById($id);
		$email = $result['email'];

		if(!CheckUserEmailAvailable($email))
		{
			return msg_fail_EmailInUse();
		}
	}

  $sql =
  "UPDATE
    user_basic
  SET
    active = :active
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':active', $active);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function CheckUserEmailAvailable($email)
{
	$sql = "SELECT id, email, created_dt FROM user_basic WHERE email = :email AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':email', $email);

		$stmt->execute();

		$db = null;

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		if(count($data) == 0)
			return true;
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

function CheckUserExist($id)
{
	$result = GetUserById($id);

  $result = json_decode($result, true);
  $count = count($result['data']);

  if($count < 1)
  {
    return true;
  }
	else
	{
		return false;
	}
}
?>
