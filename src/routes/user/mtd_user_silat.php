<?php

function GetUserSilatById($id)
{
	$sql = "SELECT * FROM user_silat WHERE id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserSilatByUserId($user_id)
{
	$sql = "SELECT * FROM user_silat WHERE user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function AddUserSilat($user_id)
{
    if(CheckUserSilatExist($user_id))
    {
      msg_fail_RecordExist();
      return;
    }

  	$sql = "INSERT INTO user_silat (user_id) VALUES (:user_id)";

    try
    {
      // Get DB Object
      $db = new db();

      // Connect
      $db = $db->connect();

      $stmt = $db->prepare($sql);

      $stmt->bindParam(':user_id', $user_id);

      $stmt->execute();

      msg_scs_RecordAdded();
    }
    catch(PDOException $e)
    {
      msg_err_Exception($e);
    }
}

function UpdateUserSilatMainKelas($user_id, $main_kelas_id)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    main_kelas_id = :main_kelas_id
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':main_kelas_id', $main_kelas_id);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserSilatPeringkat($user_id, $peringkat_id)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    peringkat_id = :peringkat_id
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':peringkat_id', $peringkat_id);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserSilatIjazah($user_id, $ijazah_asas, $ijazah_potong, $ijazah_tamat)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    ijazah_asas = :ijazah_asas,
    ijazah_potong = :ijazah_potong,
    ijazah_tamat = :ijazah_tamat
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':ijazah_asas', $ijazah_asas);
    $stmt->bindParam(':ijazah_potong', $ijazah_potong);
    $stmt->bindParam(':ijazah_tamat', $ijazah_tamat);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		returnmsg_err_Exception($e);
	}
}

function UpdateUserSilatIjazahAsas($user_id, $ijazah_asas)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    ijazah_asas = :ijazah_asas
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':ijazah_asas', $ijazah_asas);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		returnmsg_err_Exception($e);
	}
}

function UpdateUserSilatIjazahPotong($user_id, $ijazah_potong)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    ijazah_potong = :ijazah_potong
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':ijazah_potong', $ijazah_potong);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		returnmsg_err_Exception($e);
	}
}

function UpdateUserSilatIjazahTamat($user_id, $ijazah_tamat)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    ijazah_tamat = :ijazah_tamat
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':ijazah_tamat', $ijazah_tamat);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		returnmsg_err_Exception($e);
	}
}

function UpdateUserSilatSijil($user_id, $srp, $smp)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    srp = :srp,
    smp = :smp
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':srp', $srp);
    $stmt->bindParam(':smp', $smp);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserSilatSRP($user_id, $srp)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    srp = :srp
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':srp', $srp);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateUserSilatSMP($user_id, $smp)
{
  if(!CheckUserSilatExist($user_id))
  {
    msg_fail_RecordDoesntExist();
    return;
  }

	$sql =
  "UPDATE
    user_silat
  SET
    smp = :smp
  WHERE
    user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':smp', $smp);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckUserSilatExist($user_id)
{
  $sql = "SELECT * FROM user_silat WHERE user_id = :user_id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) >0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

?>
