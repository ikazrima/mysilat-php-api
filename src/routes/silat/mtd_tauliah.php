<?php

function GetSilatTauliahByKelasYear($kelas_id, $year)
{
  $sql =
  "SELECT
  	profile.id as profile_id,
  	profile.identification,
  	profile.sex,
  	profile.first_name,
  	profile.last_name,
  	profile.alternate_name,
  	tauliah.tauliah_id,
  	silat_tauliah.code,
  	silat_tauliah.jawatan,
  	socmed.facebook_id,
  	kelas.id as kelas_id,
  	kelas.code as kelas_code,
  	kelas.name as kelas_name,
  	tauliah.year
  FROM
  	user_tauliah as tauliah
  	INNER JOIN user_profile as profile
  	ON profile.id = tauliah.user_id
  	INNER JOIN silat_tauliah
  	ON silat_tauliah.id = tauliah.tauliah_id
  	LEFT JOIN social_media as socmed
  	ON socmed.owner_id = profile.id
  	INNER JOIN kelas_details as kelas
  	ON kelas.id = tauliah.kelas_id
  WHERE
    tauliah.kelas_id = :kelas_id
    AND tauliah.year = :year
    AND tauliah.active = 1
    AND profile.active = 1
  ORDER BY
    tauliah_id asc,
    profile.first_name asc,
    profile.sex desc";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':kelas_id', $kelas_id);
    $stmt->bindParam(':year', $year);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetSilatTauliahByUser($user_id)
{
  $sql =
  "SELECT
  	tauliah.tauliah_id,
  	silat_tauliah.code,
  	silat_tauliah.jawatan,
  	kelas.id as kelas_id,
  	kelas.code as kelas_code,
  	kelas.name as kelas_name,
  	tauliah.year
  FROM
  	user_tauliah as tauliah
  	INNER JOIN user_profile as profile
  	ON profile.id = tauliah.user_id
  	INNER JOIN silat_tauliah
  	ON silat_tauliah.id = tauliah.tauliah_id
  	INNER JOIN kelas_details as kelas
  	ON kelas.id = tauliah.kelas_id
  WHERE
    profile.id = :user_id
    AND tauliah.active = 1
    AND profile.active = 1
  ORDER BY
  	tauliah.year,
    tauliah_id asc";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetSilatTauliahByUserYear($user_id, $year)
{
  $sql =
  "SELECT
  	tauliah.tauliah_id,
  	silat_tauliah.code,
  	silat_tauliah.jawatan,
  	kelas.id as kelas_id,
  	kelas.code as kelas_code,
  	kelas.name as kelas_name,
    kelas.type as kelas_type,
  	tauliah.year
  FROM
  	user_tauliah as tauliah
  	INNER JOIN user_profile as profile
  	ON profile.id = tauliah.user_id
  	INNER JOIN silat_tauliah
  	ON silat_tauliah.id = tauliah.tauliah_id
  	INNER JOIN kelas_details as kelas
  	ON kelas.id = tauliah.kelas_id
  WHERE
    profile.id = :user_id
    AND tauliah.year = :year
    AND tauliah.active = 1
    AND profile.active = 1
  ORDER BY
  	tauliah.year,
    tauliah_id asc";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':year', $year);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

?>
