<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/silat/buah/all', function (Request $request, Response $response, array $args)
{
	echo GetBuahAll();
});

$app->get('/api/silat/buah/21', function (Request $request, Response $response, array $args)
{
	echo GetBuah21();
});

$app->get('/api/silat/buah/silibus', function (Request $request, Response $response, array $args)
{
	echo GetBuahSilibus();
});

$app->get('/api/silat/buah/asas', function (Request $request, Response $response, array $args)
{
	echo GetBuahAsas();
});

$app->get('/api/silat/buah/jatuh', function (Request $request, Response $response, array $args)
{
	echo GetBuahJatuh();
});

$app->get('/api/silat/buah/potong', function (Request $request, Response $response, array $args)
{
	echo GetBuahPotong();
});

$app->get('/api/silat/buah/tamat', function (Request $request, Response $response, array $args)
{
	echo GetBuahTamat();
});

$app->post('/api/silat/buah/get_jurulatih_approval', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["jurulatih_id"])
		|| empty($_POST["year"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$jurulatih_id = $request->getParam('jurulatih_id');
	$year = $request->getParam('year');

	echo GetJurulatihApproval($jurulatih_id, $year);
});

$app->post('/api/silat/buah/set_jurulatih_approval', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["jurulatih_id"])
		|| empty($_POST["approval_json"])
		|| !is_numeric($_POST["status"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$jurulatih_id = $request->getParam('jurulatih_id');
	$approval_json = $request->getParam('approval_json');
	$status = $request->getParam('status');

	echo SetJurulatihApproval($jurulatih_id, $approval_json, $status);
});
?>
