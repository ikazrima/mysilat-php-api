<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/api/silat/tauliah_kelas_year', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["kelas_id"])
		|| empty($_POST["year"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$kelas_id= $request->getParam('kelas_id');
	$year = $request->getParam('year');

	echo GetSilatTauliahByKelasYear($kelas_id, $year);
});

$app->post('/api/silat/tauliah_user', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');

	echo GetSilatTauliahByUser($user_id);
});

$app->post('/api/silat/tauliah_user_year', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
	|| empty($_POST["year"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$year = $request->getParam('year');

	echo GetSilatTauliahByUserYear($user_id, $year);
});
?>
