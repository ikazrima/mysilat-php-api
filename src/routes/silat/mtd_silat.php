<?php

// Buah

// Semua buah termasuk pecahan
function GetBuahAll()
{
  $sql = "SELECT * FROM silat_buah WHERE active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

// 21 buah TIDAK termasuk pecahan
function GetBuah21()
{
  $sql =
  "SELECT
    buah_id, buah_name, asas, jatuh, potong, tamat
  FROM
    silat_buah
  WHERE
    pecahan_id = 0 AND active = 1
    AND (tamat != 1 OR (tamat = 1 AND buah_id = '021'))";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

// Semua buah silibus termasuk pecahan
function GetBuahSilibus()
{
  $sql =
  "SELECT
    buah_id, buah_name, asas, jatuh, potong, tamat
  FROM
    silat_buah
  WHERE
    active = 1
    AND (tamat != 1 OR (tamat = 1 AND buah_id = '021'))";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

// Semua buah asas termasuk pecahan
function GetBuahAsas()
{
  $sql =
  "SELECT
    buah_id, buah_name
  FROM
    silat_buah
  WHERE
    asas = 1 AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

// Semua buah jatuh termasuk pecahan
function GetBuahJatuh()
{
  $sql =
  "SELECT
    buah_id, buah_name
  FROM
    silat_buah
  WHERE
    jatuh = 1 AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

// Semua buah potong termasuk pecahan
function GetBuahPotong()
{
  $sql =
  "SELECT
    buah_id, buah_name
  FROM
    silat_buah
  WHERE
    potong = 1 AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

// Semua buah tamat
function GetBuahTamat()
{
  $sql =
  "SELECT
    buah_id, buah_name
  FROM
    silat_buah
  WHERE
    tamat = 1 AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

// Peringkat

function GetSilatPeringkat()
{
  $sql =
  "SELECT * FROM silat_peringkat";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

// Jurulatih Approval
function GetJurulatihApproval($jurulatih_id, $year)
{
  $sql = "SELECT DISTINCT
            buah.id as user_buah_id,
            buah.user_id,
            profile.alternate_name as user_alt_name,
            image.img_path as profile_img_uri,
            social_media.facebook_id,
            image.id as profile_img_id,
    				image.img_name as profile_img_name,
    				image.img_path as profile_img_uri,
            buah.peringkat_id,
            peringkat.peringkat,
            buah.buah_json
          FROM
          	user_tauliah as tauliah
          	INNER JOIN user_buah as buah
          	ON tauliah.user_id = :jurulatih_id
            INNER JOIN user_profile as profile
      			ON profile.id = buah.user_id
      			INNER JOIN silat_peringkat as peringkat
      			ON peringkat.id = buah.peringkat_id
            INNER JOIN user_silat
      			ON user_silat.user_id = buah.user_id
      			INNER JOIN kelas_details
      			ON kelas_details.id = user_silat.main_kelas_id
            LEFT JOIN social_media
			      ON social_media.owner_id = profile.id
            LEFT JOIN image
        		ON profile.profile_img = image.id
          WHERE
          	tauliah.year >= :year - 1
            AND buah.user_id != :jurulatih_id
          ORDER BY
            user_id,
            peringkat_id desc";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':jurulatih_id', $jurulatih_id);
    $stmt->bindParam(':year', $year);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;
    //return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }

  /////////////////////////////////////////
  // decoding json
  /////////////////////////////////////////

  $approval = array();

  foreach($data as $child)
  {
    $buah_json = $child['buah_json'];
    $user_buah = json_decode($buah_json, true);

    foreach ($user_buah as $row)
    {
      if ($row['jurulatih_id'] == $jurulatih_id && $row['verified_by'] == 0 && $row['received'] > 0)
      {
          $row['user_buah_id'] = $child['user_buah_id'];
          $row['profile_id'] = $child["user_id"];
          $row['user_alt_name'] = $child["user_alt_name"];
          $row['profile_img_id'] = $child["profile_img_id"];
          $row['profile_img_name'] = $child["profile_img_name"];
          $row['profile_img_uri'] = $child["profile_img_uri"];
          $row['facebook_id'] = $child["facebook_id"];
          $row['peringkat_id'] = $child['peringkat_id'];
          $row['peringkat'] = $child['peringkat'];
          $row['kelas_id'] = $row['kelas_id'];

          $kelasData = GetKelasByIdData($row['kelas_id']);
          foreach($kelasData as $kelasRow)
          {
            $row['kelas_code'] = $kelasRow['code'];
          }

          $approval[] = $row;
      }
    }
  }

  $ord = array();
  foreach ($approval as $key => $value){
      $ord[] = strtotime($value['received_dt']);
  }
  array_multisort($ord, SORT_DESC, $approval);

  return msg_scs_Result($approval);
}

function SetJurulatihApproval($jurulatih_id, $approval_json, $status)
{
  /////////////////////////////////////////
  // select related buah_json
  /////////////////////////////////////////

  $approval = json_decode($approval_json, true);
  $valueSets = "(";

  $user_buah_id = array();

  foreach($approval as $row => $element)
  {
    $user_buah_id[] = $element['user_buah_id'];
  }

  $user_buah_id = array_unique($user_buah_id);

  foreach($user_buah_id as $row => $element)
  {
    $valueSets = $valueSets . $element;

    end($approval);

    if ($row !== key($approval) && count($user_buah_id) != 1)
      $valueSets = $valueSets . ",";
  }

  $valueSets = $valueSets .")";

  $sql = "SELECT id, buah_json FROM user_buah WHERE id IN $valueSets";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;
    // return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }

  /////////////////////////////////////////
  // update related buah_json
  /////////////////////////////////////////

  $timestamp = time();
  $date = new DateTime();
  $date->setTimestamp($timestamp);
  $date = DateTimeFormatConverter($date);

  $new_user_buah = array();

  foreach($data as $child)
  {
    $buah_json = $child['buah_json'];
    $user_buah = json_decode($buah_json, true);

    $new_buah_json = array();

    foreach ($user_buah as $row)
    {
      for($index = 0; $index < count($approval); $index++)
      {
        if($child['id'] == $approval[$index]['user_buah_id']
          && $row['buah_id'] == $approval[$index]['buah_id']
          && $row['received'] == 1)
        {
          if($status == 0)
          {
            $row['jurulatih_id'] = 0;
          }
          else if($status == 1)
          {
            $row['verified_by'] = $jurulatih_id;
            $row['verified_dt'] = $date;
          }
        }
      }

      $new_buah_json[] = $row;
    }

    $temp_copy = array();
    $temp_copy['user_buah_id'] = $child['id'];
    $temp_copy['buah_json'] = json_encode($new_buah_json);

    $new_user_buah[] = $temp_copy;
  }

  //$result = json_encode($new_user_buah);

  $sql = "";

  foreach($new_user_buah as $child)
  {
    $sql .= "UPDATE user_buah SET buah_json = '" . $child['buah_json'] . "' WHERE id = " . $child['user_buah_id'] . ";\n\n";
  }

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();
		return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}
?>
