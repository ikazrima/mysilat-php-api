<?php

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Firebase\Auth\Token\Exception\InvalidToken;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging\CloudMessage;

$GLOBALS['fcm_server_key'] = "AAAA_Ibv_yw:APA91bGfJJT4LY22O9WKk3kMJkqMfiVVNiRBQikpR3qUZrP-EPQe2ctLMhgqVqO0Suha8oZ7xINtX3YbaMYIJfbfCQz6Js_hd7uZhKmK9sCybDsoXtGTzDIFWOPxyiCjNP0iznT8_FCZ";

function AddFCM($user_id, $fcm_token, $fcm_device_id)
{
	$sql = "INSERT IGNORE  INTO user_fcm (user_id, fcm_token, fcm_device_id) VALUES (:user_id, :fcm_token, :fcm_device_id)";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
    $stmt->bindParam(':fcm_token', $fcm_token);
		$stmt->bindParam(':fcm_device_id', $fcm_device_id);

		$stmt->execute();

		return msg_scs_RecordAdded();;
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function FCMTokenValidity($token)
{
	$ch = curl_init();

	$url = 'https://iid.googleapis.com/iid/info/';

	$headers = array(
			'Content-type: application/json',
			'details: true',
			'Authorization: key='.$GLOBALS['fcm_server_key'],
	);

	// Set query data here with the URL
	curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/info/' . $token);+
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3);
	$content = trim(curl_exec($ch));
	curl_close($ch);

	$result = json_decode($content, true);

	if(isset($result['error']))
	{
		return msg_err_InvalidToken();
	}
	else
	{
		return msg_scs_Result($result);
	}
}

function FirabaseTokenValidity($token)
{
	$serviceAccount = ServiceAccount::fromJsonFile('../mysilat/src/config/mysilat-partner-588cf6080797.json');
	$firebase = (new Factory)->withServiceAccount($serviceAccount)->create();

	try {
	    $verifiedIdToken = $firebase->getAuth()->verifyIdToken($token);
	} catch (InvalidToken $e) {
	    echo $e->getMessage();
	}

	$uid = $verifiedIdToken->getClaim('sub');
	$user = $firebase->getAuth()->getUser($uid);
	echo $user;
}

// TODO
function SendFCMSingle($token, $data)
{
	$message = CloudMessage::withTarget('token', $token)
	    ->withNotification($notification) // optional
	    ->withData($data) // optional
	;

	$message = CloudMessage::fromArray([
	    'token' => $token,
	    'notification' => [/* Notification data as array */], // optional
	    'data' => [/* data array */], // optional
	]);

	$messaging->send($message);
}


?>
