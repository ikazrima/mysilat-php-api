<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/api/firebase/addfcm', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"]) || empty($_POST["fcm_token"]) || empty($_POST["fcm_device_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$fcm_token = $request->getParam('fcm_token');
	$fcm_device_id = $request->getParam('fcm_device_id');

	echo AddFCM($user_id, $fcm_token, $fcm_device_id);
});

$app->post('/api/firebase/fcmtokenvalidity', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["fcm_token"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

		$fcm_token = $request->getParam('fcm_token');
		echo FCMTokenValidity($fcm_token);
});

$app->post('/api/firebase/firebasetokenvalidity', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["firebase_token"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

		$firebase_token = $request->getParam('firebase_token');
		echo FirabaseTokenValidity($firebase_token);
});

?>
