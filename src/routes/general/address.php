<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use JSend\JSendResponse;

// Get address book by id
$app->get('/api/address/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetAddressBookById($id);
});

// Get address book by owner id AND type
$app->post('/api/address/owner_type', function (Request $request, Response $response, array $args)
{
  if (empty($_POST["owner_id"])
		|| empty($_POST["type"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}
  $owner_id= $request->getParam('owner_id');
	$type = $request->getParam('type');

	echo GetAddressBookByOwnerIdType($owner_id, $type);
});

// Add empty address only
$app->post('/api/address/add', function (Request $request, Response $response, array $args)
{
  // line2 can be left empty
	if (empty($_POST["owner_id"])
		|| empty($_POST["type"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$owner_id = $request->getParam('owner_id');
	$type = $request->getParam('type');

  echo AddAddressEmpty($owner_id, $type);
});

$app->post('/api/address/add_address', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["owner_id"])
		|| empty($_POST["type"])
		|| empty($_POST["line1"])
		|| empty($_POST["line2"])
    || empty($_POST["postcode"])
    || empty($_POST["state_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$owner_id = $request->getParam('owner_id');
	$type = $request->getParam('type');
	$line1 = $request->getParam('line1');
  $line2 = $request->getParam('line2');
  $postcode = $request->getParam('postcode');
  $state_id = $request->getParam('state_id');

	echo AddAddress($owner_id, $type, $line1, $line2, $postcode, $state_id);

});

// Update address only
$app->post('/api/address/add_phone', function (Request $request, Response $response, array $args)
{
  // home phone can be left empty
	if (empty($_POST["owner_id"])
		|| empty($_POST["type"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$owner_id = $request->getParam('owner_id');
	$type = $request->getParam('type');
	$mobile_phone = $request->getParam('mobile_phone');
  $home_phone= $request->getParam('home_phone');

  echo AddAddressPhone($id, $mobile_phone, $home_phone);
});

// Update address only
$app->post('/api/address/update_address', function (Request $request, Response $response, array $args)
{
  // line2 can be left empty
	if (empty($_POST["id"])
		|| empty($_POST["line1"])
		|| empty($_POST["line2"])
    || empty($_POST["postcode"])
    || empty($_POST["state_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$line1 = $request->getParam('line1');
  $line2 = $request->getParam('line2');
  $postcode = $request->getParam('postcode');
  $state_id = $request->getParam('state_id');

  echo UpdateAddress($id, $line1, $line2, $postcode, $state_id);
});

// Update phone only
$app->post('/api/address/update_phone', function (Request $request, Response $response, array $args)
{
  // home phone can be left empty
	if (empty($_POST["id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$mobile_phone = $request->getParam('mobile_phone');
  $home_phone= $request->getParam('home_phone');

  echo UpdateAddressPhone($id, $mobile_phone, $home_phone);
});

$app->post('/api/address/activate', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"])
	|| empty($_POST["active"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$active = $request->getParam('active');

  echo ActivateAddress($id, $active);
});

$app->get('/api/address/states/all', function (Request $request, Response $response, array $args)
{
	echo GetAllStates();
});
?>
