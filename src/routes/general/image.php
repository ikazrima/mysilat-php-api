<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/api/image/profile/save', function (Request $request, Response $response, array $args)
{
  if (empty($_POST["owner_id"])
    || empty($_POST["img_name"])
    || empty($_POST["img_path"]))
  {
    echo msg_err_ParamNotSet();
    return;
  }

	$ownerId = $request->getParam('owner_id');
	$ownerType = 1;
  $imgName = $request->getParam('img_name');
  $imgPath = $request->getParam('img_path');

  if(!CheckUserProfileExist($ownerId))
	{
    echo msg_fail_RecordDoesntExist();
    return;
  }

  $result = SaveImage($ownerId, $ownerType, $imgName, $imgPath);

  // if error
  if(!is_numeric($result))
  {
    echo $result;
    return;
  }

  echo UpdateUserProfileImg($ownerId, $result);
});

$app->get('/api/image/profile/userId/{user_id}', function (Request $request, Response $response, array $args)
{
  $userId = $request->getAttribute('user_id');

  if(!CheckUserProfileExist($userId))
	{
    echo msg_fail_RecordDoesntExist();
    return;
  }

  $result = GetUserProfileImageId($userId);

  // if error
  if(!is_numeric($result))
  {
    echo $result;
    return;
  }

  echo GetImage($result);
});


?>
