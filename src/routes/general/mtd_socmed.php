<?php

function GetSocMedById($id)
{
	$sql = "SELECT * FROM social_media WHERE id = :id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetSocMedByFBId($facebook_id)
{
	$sql = "SELECT * FROM social_media WHERE facebook_id = :facebook_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':facebook_id', $facebook_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetSocMedByTwitterId($twitter_id)
{
	$sql = "SELECT * FROM social_media WHERE twitter_id = :twitter_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':twitter_id', $twitter_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetSocMedByGoogleId($google_id)
{
	$sql = "SELECT * FROM social_media WHERE google_id = :google_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':google_id', $google_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetSocMedByOwnerId($owner_id, $ownerType)
{
	$sql = "SELECT * FROM social_media WHERE owner_id = :owner_id AND owner_type = :owner_type AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':owner_type', $ownerType);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckSocMedExistById($id)
{
  $sql = "SELECT * FROM social_media WHERE id = :id AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function CheckSocMedExistByOwnerId($owner_id)
{
  $sql = "SELECT * FROM social_media WHERE owner_id = :owner_id AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function CheckSocMedExistByFBId($id)
{
  $sql = "SELECT * FROM social_media WHERE facebook_id = :facebook_id AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':facebook_id', $facebook_id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function AddSocMed($owner_id, $type)
{
  if(CheckSocMedExistByOwnerId($owner_id))
  {
    return msg_fail_RecordExist();
  }

  $sql =
  "INSERT INTO
    social_media (owner_id, type)
  VALUES
    (:owner_id, :type)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

		$stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':type', $type);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateSocMedWebEmail($id, $website, $email_alt)
{
  if(!CheckSocMedExistById($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    social_media
  SET
    website = :website,
    email_alt = :email_alt
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':website', $website);
    $stmt->bindParam(':email_alt', $email_alt);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateSocMedFB($id, $facebook_id)
{
  if(!CheckSocMedExistById($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    social_media
  SET
    facebook_id = :facebook_id
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':facebook_id', $facebook_id);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateSocMedTwitter($id, $twitter_id)
{
  if(!CheckSocMedExistById($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    social_media
  SET
    twitter_id = :twitter_id
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':twitter_id', $twitter_id);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateSocMedGoogle($id, $google_id)
{
  if(!CheckSocMedExistById($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    social_media
  SET
    google_id = :google_id
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':google_id', $google_id);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}
?>
