<?php

function StringNumbersOnly($value)
{
  return preg_replace('/\D/', '', $value);
}

function GetGenderFromIdentification($identification)
{
	$identification = StringNumbersOnly($identification);

  // Determined by last digit in identification number
  $sex = 'f';

	if(substr($identification, -1) % 2 == 1)
		$sex = 'm';

  return $sex;
}

function ValidateIdentification($identification)
{
  $identification = StringNumbersOnly($identification);

  if(strlen($identification) == 12)
    return true;
  else
    false;
}

function ValidateEmail($email)
{
	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    return false;
	else
		return true;
}

function DateTimeFormatConverter($dt)
{
  $datetimeFormat = 'Y-m-d H:i:s';
  $date = new DateTime();
  $date = $dt->format($datetimeFormat);

  return $date;
}
?>
