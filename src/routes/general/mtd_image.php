<?php

// Don't call by url directly.
// Call through any method
function SaveImage($ownerId, $ownerType, $imgName, $imgPath)
{
	$sql =
	"INSERT INTO
		image (owner_id, owner_type, img_name, img_path)
	VALUES
		(:owner_id, :owner_type, :img_name, :img_path)";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':owner_id', $ownerId);
		$stmt->bindParam(':owner_type', $ownerType);
		$stmt->bindParam(':img_name', $imgName);
		$stmt->bindParam(':img_path', $imgPath);

		$stmt->execute();
		$id = $db->lastInsertId();

		$db = null;

		return $id;
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}


function UpdateUserProfileImg($id, $profile_img)
{
	$sql =
	"UPDATE
		user_profile
	SET
		profile_img = :profile_img
	WHERE
		id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':profile_img', $profile_img);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetImage($id)
{
	$sql =
	"SELECT *
	FROM
		image
	WHERE
		id = :id
		AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetUserProfileImageId($id)
{
	$sql =
	"SELECT profile_img
	FROM
		user_profile
	WHERE
		id = :id
		AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt-> fetch();

		$db = null;

		return $data ["profile_img"];
	}
	
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

?>
