<?php
use JSend\JSendResponse;
/////////////////////////////////
// success
/////////////////////////////////

function msg_scs_Result($data)
{
  $success = new JSendResponse('success', $data);
  return $success;
}

function msg_scs_Available()
{
  $msg = array('Available.');
  $success = new JSendResponse('success', $msg);
  return $success;
}

function msg_scs_RecordAdded()
{
  $msg = array('Record added.');
  $success = new JSendResponse('success', $msg);
  return $success;
}

function msg_scs_RecordUpdated()
{
  $msg = array('Record updated.');
  $success = new JSendResponse('success', $msg);
  return $success;
}

function msg_scs_Authenticated()
{
  $msg = array('Authenticated.');
  $success = new JSendResponse('success', $msg);
  return $success;
}

function msg_scs_EmailAvailable()
{
  $msg = array('Email available.');
  $success = new JSendResponse('success', $msg);
  return $success;
}

/////////////////////////////////
// fail
/////////////////////////////////

function msg_fail_RegistrationError()
{
  $msg = array('Registration fail. Please contact server administrator.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_RecordExist()
{
  $msg = array('Record exists.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_RecordDoesntExist()
{
  $msg = array('Record doesn\'t exists.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_InvalidLogin()
{
  $msg = array('Invalid login.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_InvalidEmail()
{
  $msg = array('Invalid email address.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_EmailInUse()
{
  $msg = array('Email already in use.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_InvalidIdentification()
{
  $msg = array('Invalid IC Number.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

function msg_fail_IdentificationInUse()
{
  $msg = array('IC Number already in use.');
  $fail = new JSendResponse('fail', $msg);
  return $fail;
}

/////////////////////////////////
// error
/////////////////////////////////

function msg_err_Exception($e)
{
  $error = new JSendResponse('error', null, $e->getMessage());
  return $error;
}

function msg_err_ParamNotSet()
{
  $msg = 'Parameter(s) not set.';
  $error = new JSendResponse('error', null, $msg);
  return $error;
}

function msg_err_InvalidJson()
{
  $msg = 'Invalid Json.';
  $error = new JSendResponse('error', null, $msg);
  return $error;
}

function msg_err_InvalidToken()
{
  $msg = 'Invalid Token.';
  $error = new JSendResponse('error', null, $msg);
  return $error;
}

?>
