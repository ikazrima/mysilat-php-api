<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/socmed/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	if($id == null)
	{
		echo msg_err_ParamNotSet();
		return;
	}

	echo GetSocMedById($id);
});

$app->get('/api/socmed/fb/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	if($id == null)
	{
		echo msg_err_ParamNotSet();
		return;
	}

	echo GetSocMedByFBId($id);
});

$app->get('/api/socmed/twitter/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	if($id == null)
	{
		echo msg_err_ParamNotSet();
		return;
	}

	echo GetSocMedByTwitterId($id);
});

$app->get('/api/socmed/google/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	if($id == null)
	{
		echo msg_err_ParamNotSet();
		return;
	}

	echo GetSocMedByGoogleId($id);
});

$app->post('/api/socmed/getowner', function (Request $request, Response $response, array $args)
{
		if (empty($_POST["owner_id"]) || empty($_POST["owner_type"]))
		{
			echo msg_err_ParamNotSet();
			return;
		}

		$ownerId = $request->getParam('owner_id');
		$ownerType = $request->getParam('owner_type');

    echo GetSocMedByOwnerId($ownerId, $ownerType);
});

$app->post('/api/socmed/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["owner_id"]) || empty($_POST["type"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$owner_id = $request->getParam('owner_id');
	$type = $request->getParam('type');

	echo AddSocMed($owner_id, $type);
});

$app->post('/api/socmed/fb_update', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$facebook_id = $request->getParam('facebook_id');

	echo UpdateSocMedFB($id, $facebook_id);
});

$app->post('/api/socmed/webemail_update', function (Request $request, Response $response, array $args)
{
  if (empty($_POST["id"])
    || empty($_POST["website"])
    || empty($_POST["email_alt"]))
  {
    echo msg_err_ParamNotSet();
    return;
  }

	$id = $request->getParam('id');
	$website = $request->getParam('website');
	$email_alt = $request->getParam('email_alt');

	echo UpdateSocMedWebEmail($id, $website, $email_alt);
});

?>
