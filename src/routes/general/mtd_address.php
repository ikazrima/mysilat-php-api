<?php

function GetAddressBookById($id)
{
  $sql = "SELECT
            address_book.id,
            address_book.owner_id,
            address_book.type,
            address_book.line1,
            address_book.line2,
            address_book.postcode,
            address_book.state_id,
            state.state,
            state.abbreviation,
            address_book.mobile_phone,
            address_book.home_phone,
            address_book.home_phone
            FROM
              address_book
            INNER JOIN
              address_state as state
            ON
              address_book.state_id = state.id
            WHERE
              address_book.id = :id";
  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetAddressBookByOwnerIdType($owner_id, $type)
{
  $sql = "SELECT * FROM address_book WHERE owner_id = :owner_id AND type = :type AND active = 1";

  $sql = "SELECT
            address_book.id,
            address_book.owner_id,
            address_book.type,
            address_book.line1,
            address_book.line2,
            address_book.postcode,
            address_book.state_id,
            state.state,
            state.abbreviation,
            address_book.mobile_phone,
            address_book.home_phone,
            address_book.home_phone
            FROM
              address_book
            LEFT JOIN
              address_state as state
            ON
              address_book.state_id = state.id
            WHERE
              address_book.owner_id = :owner_id
              AND address_book.type = :type
              AND address_book.active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':type', $type);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function AddAddressEmpty($owner_id, $type)
{
  $sql =
  "INSERT INTO
    address_book (owner_id, type)
  VALUES
    (:owner_id, :type)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);
		$stmt->bindParam(':type', $type);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function AddAddress($owner_id, $type, $line1, $line2, $postcode, $state_id)
{
  $sql =
  "INSERT INTO
    address_book (owner_id, type, line1, line2, postcode, state_id)
  VALUES
    (:owner_id, :type, :line1, :line2, :postcode, :state_id)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':line1', $type);
    $stmt->bindParam(':line2', $type);
		$stmt->bindParam(':postcode', $type);
    $stmt->bindParam(':state_id', $type);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function AddPhone($owner_id, $type, $mobile_phone, $home_phone)
{
  $sql =
  "INSERT INTO
    address_book (owner_id, type, mobile_phone, home_phone)
  VALUES
    (:owner_id, :type, :mobile_phone, :home_phone)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':mobile_phone', $mobile_phone);
		$stmt->bindParam(':home_phone', $home_phone);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateAddress($id, $line1, $line2, $postcode, $state_id)
{
  if(!CheckAddressExist($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $postcode = StringNumbersOnly($postcode);

	$sql =
  "UPDATE
    address_book
  SET
    line1 = :line1,
		line2 = :line2,
    postcode = :postcode,
    state_id = :state_id
  WHERE
    id = :id
    AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':line1', $line1);
		$stmt->bindParam(':line2', $line2);
    $stmt->bindParam(':postcode', $postcode);
		$stmt->bindParam(':state_id', $state_id);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function UpdateAddressPhone($id, $mobile_phone, $home_phone)
{
  if(!CheckAddressExist($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $mobile_phone = StringNumbersOnly($mobile_phone);
  $home_phone = StringNumbersOnly($home_phone);

	$sql =
  "UPDATE
    address_book
  SET
    mobile_phone = :mobile_phone,
		home_phone = :home_phone
  WHERE
    id = :id
    AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);
		$stmt->bindParam(':mobile_phone', $mobile_phone);
		$stmt->bindParam(':home_phone', $home_phone);

		$stmt->execute();

		$db = null;

		return msg_scs_RecordUpdated();
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function ActivateAddress($id, $active)
{
  $result = GetAddressBookById($id);

  $result = json_decode($result, true);
  $count = count($result['data']);

  if($count < 1)
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    address_book
  SET
    active = :active
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':active', $active);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function CheckAddressExist($id)
{
  $sql = "SELECT * FROM address_book WHERE id = :id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) > 0)
		{
    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

function GetAddressByOwnerId($owner_id, $type)
{
  $sql = "SELECT * FROM address_book
  WHERE
    owner_id = :owner_id
    AND type = :type
    AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':type', $type);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

    echo msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

function CheckAddressExistByOwnerId($owner_id, $type)
{
  $sql = "SELECT * FROM address_book
  WHERE
    owner_id = :owner_id
    AND type = :type
    AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':owner_id', $owner_id);
    $stmt->bindParam(':type', $type);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_OBJ);

		$db = null;

		if(count($data) > 0)
		{

    	return true;
		}
		else
			return false;
	}
	catch(PDOException $e)
	{
		echo msg_err_Exception($e);
	}
}

function GetAllStates()
{
  $sql = "SELECT * FROM address_state;";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

?>
