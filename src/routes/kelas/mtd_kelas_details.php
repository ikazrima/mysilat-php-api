<?php

function GetAllKelas()
{
	$sql = "SELECT * FROM kelas_details WHERE active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetAllKelasByStateId($state_id)
{
	$sql = "SELECT * FROM kelas_details WHERE state_id = :state_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':state_id', $state_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetAllKelasByZoneId($zone_id)
{
	$sql = "SELECT * FROM kelas_details WHERE zone_id = :zone_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':zone_id', $zone_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasById($id)
{
	$sql = "SELECT * FROM kelas_details WHERE id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasByIdData($id)
{
	$sql = "SELECT * FROM kelas_details WHERE id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return $data;
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasByName($name)
{
  $name = "%$name%";

	$sql = "SELECT * FROM kelas_details WHERE name LIKE :name AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':name', $name);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasByCode($code)
{
  $code = "%$code%";

	$sql = "SELECT * FROM kelas_details WHERE code LIKE :code AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':code', $code);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasByStateId($state_id)
{
	$sql = "SELECT * FROM kelas_details WHERE state_id = :state_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':state_id', $state_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasByZoneId($zone_id)
{
	$sql = "SELECT * FROM kelas_details WHERE zone_id = :zone_id AND active = 1";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':zone_id', $szone_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckKelasExistId($id)
{
  $sql = "SELECT * FROM kelas_details WHERE id = :id AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function CheckKelasExistCode($code)
{
  $sql = "SELECT * FROM kelas_details WHERE code = :code AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':code', $code);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function CheckKelasExistIdCode($id, $code)
{
  $sql = "SELECT * FROM kelas_details WHERE id != :id AND code = :code AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':code', $code);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function AddKelas($code, $name, $type, $state_id)
{
  if(CheckKelasExistCode($code))
  {
    return msg_fail_RecordExist();
  }

  $sql =
  "INSERT INTO
    kelas_details (code, name, type, state_id)
  VALUES
    (:code, :name, :type, :state_id)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':code', $code);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':state_id', $state_id);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function ActivateKelas($id, $active)
{
	$result = GetKelasById($id);

  $result = json_decode($result, true);
  $count = count($result['data']);

  if($count < 1)
  {
    return msg_fail_RecordDoesntExist();
  }

  $code = $result[0]['code'];

  if($active == 1)
  {
    if(CheckKelasExistCode($code))
    {
      return msg_fail_RecordExist();
    }
  }

  $sql =
  "UPDATE
    kelas_details
  SET
    active = :active
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':active', $active);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateKelas($id, $name, $type, $state_id)
{
  if(!CheckKelasExistId($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    kelas_details
  SET
    name = :name,
    type = :type,
    state_id = :state_id
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':type', $type);
    $stmt->bindParam(':state_id', $state_id);


    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateKelasCode($id, $code)
{
  if(!CheckKelasExistId($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  if(CheckKelasExistIdCode($id, $code))
  {
    return msg_fail_RecordExist();
  }

  $sql =
  "UPDATE
    kelas_details
  SET
    code = :code
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':code', $code);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetKelasMembersByYear($kelas_id, $year, $tauliah_id)
{
	$tauliah_id = json_decode($tauliah_id);
	$tauliah_id_string = implode(", ", $tauliah_id);

  $sql =
  "SELECT
  	profile.id as profile_id,
  	profile.sex,
  	profile.first_name,
  	profile.last_name,
  	profile.alternate_name,
		user_silat.peringkat_id,
		silat_peringkat.peringkat,
  	tauliah.tauliah_id,
  	silat_tauliah.code as jawatan_code,
  	silat_tauliah.jawatan,
  	socmed.facebook_id,
		tauliah.year as tauliah_year,
		image.id as profile_img_id,
		image.img_name as profile_img_name,
		image.img_path as profile_img_uri
  FROM
  	user_tauliah as tauliah
  	INNER JOIN user_profile as profile
  	ON profile.id = tauliah.user_id
  	INNER JOIN silat_tauliah
  	ON silat_tauliah.id = tauliah.tauliah_id
  	LEFT JOIN social_media as socmed
  	ON socmed.owner_id = profile.id
  	INNER JOIN kelas_details as kelas
  	ON kelas.id = tauliah.kelas_id
		INNER JOIN user_silat
		ON user_silat.user_id = profile.id
		INNER JOIN silat_peringkat
		ON silat_peringkat.id = user_silat.peringkat_id
		LEFT JOIN image
		ON profile.profile_img = image.id
  WHERE
		tauliah.tauliah_id IN ($tauliah_id_string)
    AND tauliah.kelas_id = :kelas_id
    AND tauliah.year = :year
    AND tauliah.active = 1
    AND profile.active = 1
  ORDER BY
		tauliah_id desc,
		peringkat_id desc,
		profile.sex desc,
    profile.alternate_name asc";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':kelas_id', $kelas_id);
    $stmt->bindParam(':year', $year);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

    $db = null;

    return msg_scs_Result($data);
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function GetAllKelasType()
{
	$sql = "SELECT * FROM kelas_type";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}

	function CheckKelasCodeAvailability($code)
	{
		$sql = "SELECT * FROM kelas_details WHERE code = :code";

		try
		{
			// Get DB Object
			$db = new db();

			// Connect
			$db = $db->connect();

			$stmt = $db->prepare($sql);

			$stmt->bindParam(':code', $code);

			$stmt->execute();

			$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$db = null;

			if(count($data) == 0)
				return true;
			else
				return false;
		}
		catch(PDOException $e)
		{
			return msg_err_Exception($e);
		}
	}
}
?>
