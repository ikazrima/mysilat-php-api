<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/kelas/all', function (Request $request, Response $response, array $args)
{
	echo GetAllKelas();
});

$app->get('/api/kelas/state/{state_id}', function (Request $request, Response $response, array $args)
{
  $state_id = $request->getAttribute('state_id');
	echo GetAllKelasByStateId($state_id);
});

$app->get('/api/kelas/zone/{zone_id}', function (Request $request, Response $response, array $args)
{
  $zone_id = $request->getAttribute('zone_id');
	echo GetAllKelasByZoneId($zone_id);
});

$app->get('/api/kelas/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetKelasById($id);
});

$app->get('/api/kelas/name/{name}', function (Request $request, Response $response, array $args)
{
	$name = $request->getAttribute('name');

	echo GetKelasByName($name);
});

$app->get('/api/kelas/code/{code}', function (Request $request, Response $response, array $args)
{
	$code = $request->getAttribute('code');

	echo GetKelasByCode($code);
});

$app->get('/api/kelas/code/available/{code}', function (Request $request, Response $response, array $args)
{
	$code = $request->getAttribute('code');

	if(!CheckKelasCodeAvailability($code))
	{
		echo msg_fail_RecordExist();
	}
	else
	{
		echo msg_scs_Available();
	}

});

// Add kelas entry
$app->post('/api/kelas/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["code"])
		|| empty($_POST["name"])
		|| empty($_POST["type"])
		|| empty($_POST["state_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$code = $request->getParam('code');
	$name = $request->getParam('name');
	$type = $request->getParam('type');
	$state_id = $request->getParam('state_id');

	echo AddKelas($code, $name, $type, $state_id);
});

// De/Activate kelas entry
$app->post('/api/kelas/activate', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$active = $request->getParam('active');

	ActivateKelas($id, $active);
});

// Update kelas details
$app->post('/api/kelas/update', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["kelas_id"])
		|| empty($_POST["name"])
		|| empty($_POST["type"])
		|| empty($_POST["state_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$kelasId = $request->getParam('kelas_id');
	$name = $request->getParam('name');
	$type = $request->getParam('type');
	$state_id = $request->getParam('state_id');

	echo UpdateKelas($kelasId, $name, $type, $state_id);
});

// Update kelas code
$app->post('/api/kelas/update_code', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"])
		|| empty($_POST["code"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id = $request->getParam('id');
	$code = $request->getParam('code');

	echo UpdateKelasCode($id, $code);
});

// Get kelas members by year
$app->post('/api/kelas/members_by_year', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["kelas_id"])
		|| empty($_POST["year"])
		|| empty($_POST["tauliah_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$kelas_id = $request->getParam('kelas_id');
	$year = $request->getParam('year');
	$tauliah_id = $request->getParam('tauliah_id');

	echo GetKelasMembersByYear($kelas_id, $year, $tauliah_id);
});

$app->get('/api/kelas/type/all', function (Request $request, Response $response, array $args)
{
	echo GetAllKelasType();
});
?>
