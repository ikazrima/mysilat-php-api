<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/kelas/latihan/id/{id}', function (Request $request, Response $response, array $args)
{
	$id = $request->getAttribute('id');

	echo GetKelasLatihanById($id);
});

$app->post('/api/kelas/latihan/kelas_id', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["kelas_id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$kelas_id = $request->getParam('kelas_id');

	echo GetKelasLatihanByKelasId($kelas_id);
});

$app->post('/api/kelas/latihan/userId_year', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["user_id"])
		|| empty($_POST["year"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$user_id = $request->getParam('user_id');
	$year = $request->getParam('year');

	echo GetKelasLatihanByUserIdYear($user_id, $year);
});

$app->post('/api/kelas/latihan/add', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["kelas_id"])
		|| empty($_POST["description"])
		|| empty($_POST["start_time"])
		|| empty($_POST["end_time"])
		|| empty($_POST["days"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$kelas_id= $request->getParam('kelas_id');
	$description = $request->getParam('description');
	$start_time = $request->getParam('start_time');
	$end_time = $request->getParam('end_time');
	$days = $request->getParam('days');

	echo AddKelasLatihan($kelas_id, $description, $start_time, $end_time, $days);
});

$app->post('/api/kelas/latihan/update', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"])
		|| empty($_POST["description"])
		|| empty($_POST["start_time"])
		|| empty($_POST["end_time"])
		|| empty($_POST["days"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id= $request->getParam('id');
	$description = $request->getParam('description');
	$start_time = $request->getParam('start_time');
	$end_time = $request->getParam('end_time');
	$days = $request->getParam('days');

	echo UpdateKelasLatihan($id, $description, $start_time, $end_time, $days);
});

$app->post('/api/kelas/latihan/activate', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id= $request->getParam('id');
	$active = $request->getParam('active');

	echo ActivateKelasLatihan($id, $active);
});

$app->post('/api/kelas/latihan/update_location', function (Request $request, Response $response, array $args)
{
	if (empty($_POST["id"]))
	{
		echo msg_err_ParamNotSet();
		return;
	}

	$id= $request->getParam('id');
	$latitude = $request->getParam('latitude');
	$longitude = $request->getParam('longitude');

	echo UpdateKelasLatitahanLocation($id, $latitude, $longitude);
});
?>
