<?php

function GetKelasLatihanById($id)
{
	$sql = "SELECT * FROM kelas_latihan WHERE id = :id";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasLatihanByKelasId($kelas_id)
{
	$sql = "SELECT
		latihan.id,
		latihan.description as kelas_description,
		latihan.days,
		latihan.start_time,
		latihan.end_time,
		latihan.latitude,
		latihan.longitude
		FROM
			kelas_latihan as latihan
		WHERE
			latihan.kelas_id = :kelas_id
			AND latihan.active = 1
		ORDER BY
			latihan.days,
			latihan.start_time";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

    $stmt->bindParam(':kelas_id', $kelas_id);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function GetKelasLatihanByUserIdYear($user_id, $year)
{
	$sql = "SELECT
		kelas.id as kelas_id,
		kelas.code as kelas_code,
		kelas.name as kelas_name,
		latihan.description as kelas_description,
		latihan.days,
		latihan.start_time,
		latihan.end_time,
		latihan.latitude,
		latihan.longitude
	FROM
		user_tauliah as tauliah
		INNER JOIN user_profile as profile
		ON profile.id = tauliah.user_id
		INNER JOIN silat_tauliah
		ON silat_tauliah.id = tauliah.tauliah_id
		INNER JOIN kelas_details as kelas
		ON kelas.id = tauliah.kelas_id
		LEFT JOIN kelas_latihan as latihan
		ON latihan.kelas_id = kelas.id
	WHERE
		profile.id = :user_id
		AND tauliah.year = :year
		AND profile.active = 1
		AND tauliah.active = 1
		AND latihan.active = 1
	ORDER BY
		latihan.days,
		latihan.start_time";

	try
	{
		// Get DB Object
		$db = new db();

		// Connect
		$db = $db->connect();

		$stmt = $db->prepare($sql);

		$stmt->bindParam(':user_id', $user_id);
		$stmt->bindParam(':year', $year);

		$stmt->execute();

		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$db = null;

		return msg_scs_Result($data);
	}
	catch(PDOException $e)
	{
		return msg_err_Exception($e);
	}
}

function CheckKelasLatihanExistId($id)
{
  $sql = "SELECT * FROM kelas_latihan WHERE id = :id AND active = 1";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $db = null;

    if(count($data) > 0)
			return true;
		else
			return false;
  }
  catch(PDOException $e)
  {
    echo msg_err_Exception($e);
  }
}

function AddKelasLatihan($kelas_id, $description, $start_time, $end_time, $days)
{
  $sql =
  "INSERT INTO
    kelas_latihan (kelas_id, description, start_time, end_time, days)
  VALUES
    (:kelas_id, :description, :start_time, :end_time, :days)";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':kelas_id', $kelas_id);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':start_time', $start_time);
    $stmt->bindParam(':end_time', $end_time);
    $stmt->bindParam(':days', $days);

    $stmt->execute();

    return msg_scs_RecordAdded();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateKelasLatihan($id, $description, $start_time, $end_time, $days)
{
  if(!CheckKelasLatihanExistId($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    kelas_latihan
  SET
    description = :description,
    start_time = :start_time,
    end_time = :end_time,
    days = :days
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':description', $description);
    $stmt->bindParam(':start_time', $start_time);
    $stmt->bindParam(':end_time', $end_time);
    $stmt->bindParam(':days', $days);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function UpdateKelasLatitahanLocation($id, $latitude, $longitude)
{
  if(!CheckKelasLatihanExistId($id))
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    kelas_latihan
  SET
    latitude = :latitude,
    longitude = :longitude
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':latitude', $latitude);
    $stmt->bindParam(':longitude', $longitude);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}

function ActivateKelasLatihan($id, $active)
{
	$result = GetKelasLatihanById($id);

  $result = json_decode($result, true);
  $count = count($result['data']);

  if($count < 1)
  {
    return msg_fail_RecordDoesntExist();
  }

  $sql =
  "UPDATE
    kelas_latihan
  SET
    active = :active
  WHERE
    id = :id";

  try
  {
    // Get DB Object
    $db = new db();

    // Connect
    $db = $db->connect();

    $stmt = $db->prepare($sql);

    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':active', $active);

    $stmt->execute();

    $db = null;

    return msg_scs_RecordUpdated();
  }
  catch(PDOException $e)
  {
    return msg_err_Exception($e);
  }
}
?>
