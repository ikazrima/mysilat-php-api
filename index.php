<?php
	require '../vendor/autoload.php';
	require 'src/config/config.php';

	$app = new \Slim\App;

	// User
	require 'src/routes/user/user_account.php';
	require 'src/routes/user/user_basic.php';
	require 'src/routes/user/user_profile.php';
	require 'src/routes/user/user_buah.php';
	require 'src/routes/user/user_silat.php';
	require 'src/routes/user/user_tauliah.php';

	require 'src/routes/user/mtd_user_account.php';
	require 'src/routes/user/mtd_user_basic.php';
	require 'src/routes/user/mtd_user_profile.php';
	require 'src/routes/user/mtd_user_buah.php';
	require 'src/routes/user/mtd_user_silat.php';
	require 'src/routes/user/mtd_user_tauliah.php';

	// Silat
	require 'src/routes/silat/silat_buah.php';
	require 'src/routes/silat/silat_peringkat.php';
	require 'src/routes/silat/silat_tauliah.php';

	require 'src/routes/silat/mtd_silat.php';
	require 'src/routes/silat/mtd_tauliah.php';

	// Kelas
	require 'src/routes/kelas/kelas_details.php';
	require 'src/routes/kelas/kelas_latihan.php';

	require 'src/routes/kelas/mtd_kelas_details.php';
	require 'src/routes/kelas/mtd_kelas_latihan.php';

	// General
	require 'src/routes/general/response_msg.php';

	require 'src/routes/general/address.php';
	require 'src/routes/general/socmed.php';
	require 'src/routes/general/image.php';

	require 'src/routes/general/mtd_helper.php';
	require 'src/routes/general/mtd_address.php';
	require 'src/routes/general/mtd_socmed.php';
	require 'src/routes/general/mtd_image.php';

	// Firebase
	require 'src/routes/firebase/firebase.php';
	require 'src/routes/firebase/mtd_firebase.php';

	$app->run();
?>
