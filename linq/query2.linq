<Query Kind="SQL">
  <Connection>
    <ID>783489b2-e80a-4d53-9bcb-a2f8532ab144</ID>
    <Persist>true</Persist>
    <Driver Assembly="IQDriver" PublicKeyToken="5b59726538a49684">IQDriver.IQDriver</Driver>
    <Provider>Devart.Data.MySql</Provider>
    <CustomCxString>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAYUmPzV0WiEqzdaVyj9xfZwAAAAACAAAAAAAQZgAAAAEAACAAAADXBw0inLxRWWn6Q8crui69oq2bHw9jUTCIV7JcYavrwAAAAAAOgAAAAAIAACAAAAB0dKUmFGovpy+d8Xpx3YSUVXtt53ahZP6jVzzGNuRJkFAAAAANg1jRJZX9lSuRcSZdAku9haUA2Y8ok51rvGdx2pn/USqe46TlhEGWZ8j7V2J0XktoMhnr2NkPL415/tP/XcoNmc8gqPrRtA8r+nakyeH3QkAAAADB+JZ04xGtgJlOAUhaUt1Eamp85jEw4hhxmBuOeKWnWoMFO8B+9tlCi7e70JwIiCF5uwDf029UTRrww7+GLLA7</CustomCxString>
    <Database>atherons_mysilat</Database>
    <Server>localhost</Server>
    <UserName>root</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAYUmPzV0WiEqzdaVyj9xfZwAAAAACAAAAAAAQZgAAAAEAACAAAACzoepwUhijjY5jv2DsN+3sGeoKi/gEEOZcyHtgaheGLwAAAAAOgAAAAAIAACAAAAB3gyLdUTJ4ynZhUZZXQjXt8/ClcRWCPRM5einFwW+PDBAAAACxLyDBqxMirh/g/3WPAES6QAAAAEoMqk8Z2zdqEsUaEDvt4R7PiYBrTY/PbyWi0xup6EwnYJMaIrfqKCMish56SgJlLNe8hFYv7t1Ipt6fiBzJdAU=</Password>
    <DisplayName>mysilat localhost</DisplayName>
    <EncryptCustomCxString>true</EncryptCustomCxString>
    <DriverData>
      <StripUnderscores>false</StripUnderscores>
      <QuietenAllCaps>false</QuietenAllCaps>
    </DriverData>
  </Connection>
</Query>

SELECT
	profile.id as profile_id,
	profile.identification,
	profile.sex,
	profile.first_name,
	profile.last_name,
	profile.alternate_name,
	tauliah.tauliah_id,
	silat_tauliah.code,
	silat_tauliah.jawatan,
	socmed.facebook_id,
	kelas.id as kelas_id,
	kelas.code as kelas_code,
	kelas.name as kelas_name,
	tauliah.year
FROM
	user_tauliah as tauliah
	INNER JOIN user_profile as profile
	ON profile.id = tauliah.user_id
	INNER JOIN silat_tauliah
	ON silat_tauliah.id = tauliah.tauliah_id
	LEFT JOIN social_media as socmed
	ON socmed.owner_id = profile.id
	INNER JOIN kelas_details as kelas
	ON kelas.id = tauliah.kelas_id
WHERE
	tauliah.kelas_id = 1
	AND tauliah.year = 2010
	AND tauliah.active = 1
	AND profile.active = 1
ORDER BY
	tauliah_id asc,
	profile.first_name asc,
	profile.sex desc