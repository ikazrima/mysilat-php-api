<Query Kind="SQL">
  <Connection>
    <ID>64b7cb4c-6325-41ea-80f3-59e7b142fffd</ID>
    <Persist>true</Persist>
    <Driver Assembly="IQDriver" PublicKeyToken="5b59726538a49684">IQDriver.IQDriver</Driver>
    <Provider>Devart.Data.MySql</Provider>
    <CustomCxString>Server=localhost;Database=atherons_mysilat;Uid=root</CustomCxString>
    <Server>localhost</Server>
    <Database>atherons_mysilat</Database>
    <UserName>root</UserName>
    <DriverData>
      <StripUnderscores>false</StripUnderscores>
      <QuietenAllCaps>false</QuietenAllCaps>
    </DriverData>
  </Connection>
</Query>

set @jurulatih_id = 1;
 set @year = 2019;
 
 SELECT DISTINCT
          	buah.id as user_buah_id,
          	buah.user_id,
			profile.alternate_name as user_alt_name,
			social_media.facebook_id,
            buah.peringkat_id,
			peringkat.peringkat,
			kelas_details.id as kelas_id,
			kelas_details.code as kelas_code,
          	buah.buah_json
          FROM
          	user_tauliah as tauliah
          	INNER JOIN user_buah as buah
          	ON tauliah.user_id = @jurulatih_id
			INNER JOIN user_profile as profile
			ON profile.id = buah.user_id
			INNER JOIN silat_peringkat as peringkat
			ON peringkat.id = buah.peringkat_id
			INNER JOIN user_silat
			ON user_silat.user_id = buah.user_id
			INNER JOIN kelas_details
			ON kelas_details.id = user_silat.main_kelas_id
			LEFT JOIN social_media
			ON social_media.owner_id = profile.id
          WHERE
          	tauliah.year >= @year - 1
            AND buah.user_id != @jurulatih_id
          ORDER BY
          	user_id,
          	peringkat_id desc