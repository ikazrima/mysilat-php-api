<Query Kind="SQL">
  <Connection>
    <ID>783489b2-e80a-4d53-9bcb-a2f8532ab144</ID>
    <Persist>true</Persist>
    <Driver Assembly="IQDriver" PublicKeyToken="5b59726538a49684">IQDriver.IQDriver</Driver>
    <Provider>Devart.Data.MySql</Provider>
    <CustomCxString>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAYUmPzV0WiEqzdaVyj9xfZwAAAAACAAAAAAAQZgAAAAEAACAAAADXBw0inLxRWWn6Q8crui69oq2bHw9jUTCIV7JcYavrwAAAAAAOgAAAAAIAACAAAAB0dKUmFGovpy+d8Xpx3YSUVXtt53ahZP6jVzzGNuRJkFAAAAANg1jRJZX9lSuRcSZdAku9haUA2Y8ok51rvGdx2pn/USqe46TlhEGWZ8j7V2J0XktoMhnr2NkPL415/tP/XcoNmc8gqPrRtA8r+nakyeH3QkAAAADB+JZ04xGtgJlOAUhaUt1Eamp85jEw4hhxmBuOeKWnWoMFO8B+9tlCi7e70JwIiCF5uwDf029UTRrww7+GLLA7</CustomCxString>
    <Database>atherons_mysilat</Database>
    <Server>localhost</Server>
    <UserName>root</UserName>
    <Password>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAYUmPzV0WiEqzdaVyj9xfZwAAAAACAAAAAAAQZgAAAAEAACAAAACzoepwUhijjY5jv2DsN+3sGeoKi/gEEOZcyHtgaheGLwAAAAAOgAAAAAIAACAAAAB3gyLdUTJ4ynZhUZZXQjXt8/ClcRWCPRM5einFwW+PDBAAAACxLyDBqxMirh/g/3WPAES6QAAAAEoMqk8Z2zdqEsUaEDvt4R7PiYBrTY/PbyWi0xup6EwnYJMaIrfqKCMish56SgJlLNe8hFYv7t1Ipt6fiBzJdAU=</Password>
    <DisplayName>mysilat localhost</DisplayName>
    <EncryptCustomCxString>true</EncryptCustomCxString>
    <DriverData>
      <StripUnderscores>false</StripUnderscores>
      <QuietenAllCaps>false</QuietenAllCaps>
    </DriverData>
  </Connection>
</Query>

SELECT
	socmed.owner_id as profile_id,
	profile.basic_id as basic_id,
	profile.identification,
	profile.first_name,
	profile.last_name,
	profile.alternate_name,
	profile.sex,
	profile.address_id,
	profile.profile_img
FROM
	user_profile as profile
	INNER JOIN social_media as socmed
	ON socmed.owner_id = profile.id
	INNER JOIN user_basic as basic
	ON basic.id = profile.basic_id
WHERE
	profile.active = 1
	AND socmed.type = 'user'
	AND socmed.facebook_id = '1597113543659216'