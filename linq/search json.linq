<Query Kind="SQL">
  <Connection>
    <ID>64b7cb4c-6325-41ea-80f3-59e7b142fffd</ID>
    <Persist>true</Persist>
    <Driver Assembly="IQDriver" PublicKeyToken="5b59726538a49684">IQDriver.IQDriver</Driver>
    <Provider>Devart.Data.MySql</Provider>
    <CustomCxString>Server=localhost;Database=atherons_mysilat;Uid=root</CustomCxString>
    <Server>localhost</Server>
    <Database>atherons_mysilat</Database>
    <UserName>root</UserName>
    <DriverData>
      <StripUnderscores>false</StripUnderscores>
      <QuietenAllCaps>false</QuietenAllCaps>
    </DriverData>
  </Connection>
</Query>

SET @jurulatih_id = '"jurulatih_id":';
SET @jurulatih_id_length = LENGTH(@jurulatih_id) + 1;

SET @buah_id = '"buah_id":';
SET @buah_id_length = LENGTH(@buah_id) + 1;

SET @received_dt = '"received_dt":';
SET @received_dt_length = CHAR_LENGTH(@received_dt) + 1;

SET @verified_by = '"verified_by":';
SET @verified_by_length = LENGTH(@verified_by) + 1;

SET @verified_dt = '"verified_dt":';
SET @verified_dt_length = CHAR_LENGTH(@verified_dt) + 1;

SELECT * FROM
(
	SELECT
		id,
		user_id,
		peringkat_id,
		
		(substring(buah_json, locate(@buah_id, buah_json)+@buah_id_length,
	     locate('","', buah_json, locate(@buah_id, buah_json))-locate(@buah_id, buah_json)-@buah_id_length)
		 
	  ) as buah_id,
		
	  (substring(buah_json, locate(@jurulatih_id, buah_json)+@jurulatih_id_length,
	     locate('","', buah_json, locate(@jurulatih_id, buah_json))-locate(@jurulatih_id, buah_json)-@jurulatih_id_length)
		 
	  ) as received_from_jurulatih_id,

	  (substring(buah_json, locate(@received_dt, buah_json)+@received_dt_length,
	     locate('","', buah_json, locate(@received_dt, buah_json))-locate(@received_dt, buah_json)-@received_dt_length)
		 
	  ) as received_dt,

	  (substring(buah_json, locate(@verified_by, buah_json)+@verified_by_length,
	     locate('","', buah_json, locate(@verified_by, buah_json))-locate(@verified_by, buah_json)-@verified_by_length)
		 
	  ) as verified_by,
	  
	  (substring(buah_json, locate(@verified_dt, buah_json)+@verified_dt_length,
	     locate('","', buah_json, locate(@verified_dt, buah_json))-locate(@verified_dt, buah_json)-@verified_dt_length - 18)
		 
	  ) as verified_dt
	FROM
	  user_buah
	ORDER BY
		peringkat_id,
		buah_id
	) t
/*WHERE
	received_from_jurulatih_id = 1*/
	
-- select * from user_buah